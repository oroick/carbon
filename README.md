# Carbon Test

## Setup

Requires:

- Python 3.x
- Django 3.x

You can install the requirements using:

```
pip install -r requirements.txt
```

This will install the Django version I used. 

## Running

Start the local development server using:

```
./manage.py runserver
```

## What I have built

I built a simple Web application that allows you to upload datasets and depict charts of the meter readings for individual buildings. 

The main pages are:

- http://localhost:8000/upload/, where you can upload the CSV files to write the datasets into the database. To upload data, pick a file from your file system, and select the corresponding datatype. **Note** The data needs to be uploaded in the following order: Buildings, meters, and then readings; otherwise, that will throw integrity errors because the foreign-key relationships cannot be set. 
- http://localhost:8000/buildings/ list all buildings, including a link to the building detail page. Each detail page lists the meters registered for the building and a chart depicting natural-gas and electricity consumption. The chart shows aggregated data per, i.e., if a building has two electricity meters, the readings of meters are summed up for each day. 

## Trade-offs

Given the tight timeframe and the fact that I still have a full-time job, I decided to make some trade-offs to be able to deliver value on time:

- I went for a basic UI without any CSS. I'm not a designed and I wanted to focus on building the functionality. 
- I haven't written any tests. I treated this project as a proof of concept and decided to skip tests to get a working solution quickly. I would not ship untested code to production. In this solution, some critical parts definitely need tests, such as the form validation, data parsing, and storing data in the database. 
- Some user journeys will lead to errors. For example, if you upload a CSV twice or upload the meter data before the building data, you'll get integrity errors. These errors are not handled but should be as I'm expecting these to be quite common errors. 
- The data-upload functionality is pretty basic. Uploading the meter readings already takes considerable time. You are probably dealing with way larger data sets than the ones provided – an asynchronous solution may be more appropriate if this was a production solution. 
- I picked SQLite for the database, to make setup easier. In a production system, I would choose an RDBMS better suited to hold large datasets, such as Postgres.
- Add a legend to the charts. As is the charts are unreadable as there is no indication of what the charts mean. (Red = Gas consumption, Green = electricity consumption)
