import json

from django.views.generic.edit import FormView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView

from django.db.models import Sum, Q
from django.db.models.functions import TruncDay
from django.core.serializers.json import DjangoJSONEncoder

from . import forms, models
from .data import store_data


class Upload(FormView):
    template_name = 'upload.html'
    form_class = forms.Upload
    success_url = '/upload/'

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)

        if form.is_valid():
            store_data(request.POST['data_type'], request.FILES['data_file'])
            return self.form_invalid(form)
        else:
            return self.form_invalid(form)


class Buildings(ListView):
    model = models.Building
    template_name = 'buildings.html'


class BuildingDetail(DetailView):
    model = models.Building
    template_name = 'meters.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        meters = models.Meter.objects.filter(building_id=context['building'].id)
        context['meters'] = meters

        meter_ids = [m.id for m in meters]
        daily_consumption = (
            models.MeterReading.objects
                .filter(meter_id__in=meter_ids)
                .annotate(day=TruncDay('reading_date_time'))
                .values('day')
                .annotate(
                    total_water=Sum('consumption', filter=Q(meter__fuel='Water')),
                    total_gas=Sum('consumption', filter=Q(meter__fuel='Natural Gas')),
                    total_electricity=Sum('consumption', filter=Q(meter__fuel='Electricity'))
                )
        )
        context['daily_consumption'] = json.dumps(list(daily_consumption), cls=DjangoJSONEncoder)

        return context
