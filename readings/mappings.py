from collections import namedtuple
from . import models

Dataset = namedtuple('DataSet', ['model', 'fields'])

buildings = Dataset(
    model=models.Building,
    fields=('id', 'name', )
)

meters = Dataset(
    model=models.Meter,
    fields=('id', 'building_id', 'fuel', 'unit')
)

readings = Dataset(
    model=models.MeterReading,
    fields=('consumption', 'meter_id', 'reading_date_time')
)
