fuel = (
    ('Electricity', 'Electricity'),
    ('Natural Gas', 'Natural Gas'),
    ('Water', 'Water'),
)

units = (
    ('m3', 'm3'),
    ('kwh', 'kwh'),
)


data_types = (
    ('buildings', 'Buildings'),
    ('meters', 'Meters'),
    ('readings', 'Meter readings')
)
