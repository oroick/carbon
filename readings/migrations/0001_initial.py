# Generated by Django 3.0.5 on 2020-04-21 17:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Building',
            fields=[
                ('id', models.PositiveIntegerField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Meter',
            fields=[
                ('id', models.PositiveIntegerField(primary_key=True, serialize=False)),
                ('fuel', models.CharField(choices=[('Electricity', 'Electricity'), ('Natural Gas', 'Natural Gas'), ('Water', 'Water')], max_length=15)),
                ('unit', models.CharField(choices=[('m3', 'm3'), ('kwh', 'kwh')], max_length=5)),
                ('building', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='readings.Building')),
            ],
        ),
        migrations.CreateModel(
            name='MeterReading',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('consumption', models.DecimalField(decimal_places=5, max_digits=10)),
                ('reading_date_time', models.DateTimeField()),
                ('meter', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='readings.Meter')),
            ],
        ),
    ]
