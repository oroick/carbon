from django.db import models
from . import choices


class Building(models.Model):
    id = models.PositiveIntegerField(primary_key=True)
    name = models.CharField(max_length=255)


class Meter(models.Model):
    id = models.PositiveIntegerField(primary_key=True)
    fuel = models.CharField(
        max_length=15,
        choices=choices.fuel
    )
    unit = models.CharField(
        max_length=5,
        choices=choices.units
    )

    building = models.ForeignKey('readings.Building', on_delete=models.CASCADE)


class MeterReading(models.Model):
    consumption = models.DecimalField(max_digits=10, decimal_places=5)
    reading_date_time = models.DateTimeField()

    meter = models.ForeignKey('readings.Meter', on_delete=models.CASCADE)
