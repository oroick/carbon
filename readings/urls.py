from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^upload/$', views.Upload.as_view(), name='upload'),
    url(r'^buildings/$', views.Buildings.as_view(), name='buildings'),
    url(r'^buildings/(?P<pk>\d+)/?$', views.BuildingDetail.as_view(), name='building_detail'),
]
