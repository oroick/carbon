import csv
import codecs

from . import mappings


def store_data(data_type, src_file):
    mapping = getattr(mappings, data_type)
    reader = csv.DictReader(codecs.iterdecode(src_file, 'utf-8-sig'), delimiter=',')

    objects = [
        mapping.model(**{field: record[field] for field in mapping.fields})
        for record in reader
        if all(record[field] for field in mapping.fields)
    ]
    mapping.model.objects.bulk_create(objects)
