from django import forms
import csv
import codecs

from . import choices, mappings


class Upload(forms.Form):
    data_file = forms.FileField()
    data_type = forms.ChoiceField(choices=choices.data_types)

    def clean_data_file(self):
        data_type = self.data['data_type']
        f = self.cleaned_data['data_file']
        mapping = getattr(mappings, data_type)

        reader = csv.DictReader(codecs.iterdecode(f, 'utf-8-sig'), delimiter=',')

        actual_cols = list(filter(lambda field: field, reader.fieldnames))
        if not all(col in actual_cols for col in mapping.fields):
            raise forms.ValidationError(
                f"The CSV '{f.name}' does not have the correct format for type '{data_type}'"
            )

        return f
